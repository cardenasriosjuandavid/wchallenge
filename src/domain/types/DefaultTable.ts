export type DefaultTable = {
	active?: boolean
	timestampCreation?: string
	timestampModify?: string
}

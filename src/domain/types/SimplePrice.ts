export interface ISimplePrice {
	[key: string]: { ars: number; usd: number; eur: number }
}

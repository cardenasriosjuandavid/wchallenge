import { DefaultTable } from './DefaultTable'

export interface IUser extends DefaultTable {
	id?: number
	name: string
	surname: string
	username: string
	password: string
	currency: 'ars' | 'eur' | 'usd'
}

import CryptoDomainRespository from '../Crypto'

const repository = new CryptoDomainRespository()

describe('CryptoDomainRespository', () => {
	it('Method not implemented in `getCryptoByUser`', () => {
		return expect(() => {
			repository.getCryptoByUser('random', 'juancho')
		}).toThrowError('Method not implemented')
	})

	it('Method not implemented in `toThrowError`', () => {
		return expect(() => {
			repository.saveCrypto({ id: undefined, idCrypto: '', username: '' })
		}).toThrowError('Method not implemented')
	})
})

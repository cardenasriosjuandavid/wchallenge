import CryptoClientDomain from '../CryptoClient'

const domain = new CryptoClientDomain()

describe('CryptoClientDomain', () => {
	it('Method not implemented in `getAll`', () => {
		return expect(() => {
			domain.getAll('ars')
		}).toThrowError('Method not implemented')
	})

	it('Method not implemented in `getCryptoById`', () => {
		return expect(() => {
			domain.getCryptoById('invalid')
		}).toThrowError('Method not implemented')
	})
})

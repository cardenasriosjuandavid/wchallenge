import UserEntity from '../../entities/User'
import UserDomainRepository from '../User'

const domain = new UserDomainRepository()

describe('UserDomainRepository', () => {
	it('Method not implemented in `create`', () => {
		return expect(() => {
			domain.create(
				new UserEntity({
					currency: 'ars',
					name: 'John',
					surname: 'Doe',
					password: 'password',
					username: 'jhondoe',
				})
			)
		}).toThrowError('Method not implemented')
	})

	it('Method not implemented in `getByUsername`', () => {
		return expect(() => {
			domain.getByUsername('invalid')
		}).toThrowError('Method not implemented')
	})
})

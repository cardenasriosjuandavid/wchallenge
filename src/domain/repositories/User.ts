// Entity
import UserEntity from '../entities/User'

// Handle Errors
import APIError from '../../infrastructure/handler/APIErrorHandler'
import HttpStatusCode from '../../infrastructure/handler/HttpStatusCode'

class UserDomainRepository {
	private error: string

	private message: string

	constructor() {
		this.error = 'ERR_METHOD_NOT_IMPLEMENTED'
		this.message = 'Method not implemented'
	}

	// eslint-disable-next-line @typescript-eslint/no-unused-vars
	getByUsername(username: string): void {
		throw new APIError(this.error, HttpStatusCode.NOT_FOUND, this.message)
	}

	// eslint-disable-next-line @typescript-eslint/no-unused-vars
	create(user: UserEntity): void {
		throw new APIError(this.error, HttpStatusCode.NOT_FOUND, this.message)
	}
}

export default UserDomainRepository

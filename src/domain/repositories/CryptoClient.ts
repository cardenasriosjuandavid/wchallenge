// Handle Errors
import APIError from '../../infrastructure/handler/APIErrorHandler'
import HttpStatusCode from '../../infrastructure/handler/HttpStatusCode'

class CryptoClientDomain {
	error: string

	message: string

	constructor() {
		this.error = 'ERR_METHOD_NOT_IMPLEMENTED'
		this.message = 'Method not implemented'
	}

	// eslint-disable-next-line @typescript-eslint/no-unused-vars
	getAll(currency: 'ars' | 'eur' | 'usd'): void {
		throw new APIError(this.error, HttpStatusCode.NOT_FOUND, this.message)
	}

	// eslint-disable-next-line @typescript-eslint/no-unused-vars
	getCryptoById(idCrypto: string): void {
		throw new APIError(this.error, HttpStatusCode.NOT_FOUND, this.message)
	}
}

export default CryptoClientDomain

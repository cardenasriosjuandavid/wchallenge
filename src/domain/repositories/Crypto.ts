// Entity
import UserCryptoEntity from '../entities/UserCrypto'

// Handle Errors
import HttpStatusCode from '../../infrastructure/handler/HttpStatusCode'
import APIError from '../../infrastructure/handler/APIErrorHandler'

class CryptoDomainRespository {
	private error: string

	private message: string

	constructor() {
		this.error = 'ERR_METHOD_NOT_IMPLEMENTED'
		this.message = 'Method not implemented'
	}

	// eslint-disable-next-line @typescript-eslint/no-unused-vars
	saveCrypto(userCrypt: UserCryptoEntity): void {
		throw new APIError(this.error, HttpStatusCode.NOT_FOUND, this.message)
	}

	// eslint-disable-next-line @typescript-eslint/no-unused-vars
	getCryptoByUser(idCrypto: string, username: string): void {
		throw new APIError(this.error, HttpStatusCode.NOT_FOUND, this.message)
	}
}

export default CryptoDomainRespository

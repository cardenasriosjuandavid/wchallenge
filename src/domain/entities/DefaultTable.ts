import { DefaultTable } from '../types/DefaultTable'

class DefaultEntity {
	public active: boolean | undefined

	public timestampCreation: string | undefined

	public timestampModify: string | undefined

	constructor({ active, timestampCreation, timestampModify }: DefaultTable) {
		this.active = active
		this.timestampCreation = timestampCreation
		this.timestampModify = timestampModify
	}
}

export default DefaultEntity

import { IUser } from '../types/User'
import DefaultEntity from './DefaultTable'

class UserEntity extends DefaultEntity {
	public id: number | undefined

	public name: string

	public surname: string

	public username: string

	public password: string

	public currency: 'ars' | 'eur' | 'usd'

	constructor({
		id,
		name,
		surname,
		username,
		password,
		currency,
		active,
		timestampCreation,
		timestampModify,
	}: IUser) {
		super({ active, timestampCreation, timestampModify })
		this.id = id
		this.name = name
		this.surname = surname
		this.username = username
		this.password = password
		this.currency = currency
	}
}

export default UserEntity

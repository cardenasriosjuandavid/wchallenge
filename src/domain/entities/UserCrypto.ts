import { IUserCrypto } from '../types/UserCrypto'

class UserCryptoEntity {
	public id: number | undefined

	public idCrypto: string

	public username: string

	constructor({ id, idCrypto, username }: IUserCrypto) {
		this.id = id
		this.idCrypto = idCrypto
		this.username = username
	}
}

export default UserCryptoEntity

import CryptoRespository from '../repositories/Crypto'
import CryptoClientRepository from '../repositories/CryptoClient'
import UserRepository from '../repositories/User'
import JwtAccessTokenManager from '../security/jwt'

const buildBeans = () => ({
	userRepository: new UserRepository(),
	accessTokenManager: new JwtAccessTokenManager(),
	cryptoClientRepository: new CryptoClientRepository(),
	cryptoRespository: new CryptoRespository(),
})

export default buildBeans()

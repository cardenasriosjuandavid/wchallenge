import sequelize from '../orm/sequelize'

export default {
	async init(): Promise<void> {
		try {
			await sequelize.authenticate()
			console.log('Connection to DB has been established successfully.')
		} catch (err) {
			console.error('Unable to connect to the database:', err.message)
		}
	},
}

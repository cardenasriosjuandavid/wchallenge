import { Sequelize } from 'sequelize'
import dotenv from 'dotenv'

dotenv.config()

const { HOST_DB, USER_DB, PASSWORD_DB, SCHEMA_DB } = process.env

const sequelize = new Sequelize({
	dialect: 'mysql',
	host: HOST_DB,
	username: USER_DB,
	password: PASSWORD_DB,
	database: SCHEMA_DB,
	logging: false,
	dialectOptions: { decimalNumbers: true, supportBigNumbers: true },
})

export default sequelize

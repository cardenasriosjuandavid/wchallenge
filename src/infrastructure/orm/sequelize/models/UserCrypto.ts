import { DataTypes, Model } from 'sequelize'

import sequelize from '..'

// @Types
import { IUserCrypto } from '../../../../domain/types/UserCrypto'

// We need to declare an interface for our model that is basically what our class would be
interface UserCryptoInstance extends Model<IUserCrypto>, IUserCrypto {}

const UserCryptoModel = sequelize.define<UserCryptoInstance>(
	'UserCrypto',
	{
		id: {
			type: DataTypes.INTEGER,
			primaryKey: true,
			autoIncrement: true,
			allowNull: false,
			field: 'id_user_crypto',
		},
		idCrypto: {
			type: DataTypes.STRING(45),
			primaryKey: true,
			allowNull: false,
			field: 'id_crypto',
		},
		username: {
			type: DataTypes.STRING(50),
			allowNull: false,
			field: 'fk_username_user',
		},
	},
	{
		tableName: 'tbl_user_crypto',
		timestamps: false,
	}
)

export default UserCryptoModel

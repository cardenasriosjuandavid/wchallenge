import { DataTypes, Model, Sequelize } from 'sequelize'

import sequelize from '..'

// @Types
import { IUser } from '../../../../domain/types/User'

// We need to declare an interface for our model that is basically what our class would be
interface UserInstance extends Model<IUser>, IUser {}

const UserModel = sequelize.define<UserInstance>(
	'User',
	{
		id: {
			type: DataTypes.INTEGER,
			primaryKey: true,
			autoIncrement: true,
			allowNull: false,
			field: 'id_user',
		},
		name: {
			type: DataTypes.STRING(50),
			allowNull: false,
			field: 'name_user',
		},
		surname: {
			type: DataTypes.STRING(250),
			defaultValue: null,
			field: 'surname_user',
		},
		username: {
			type: DataTypes.STRING(250),
			allowNull: false,
			field: 'username_user',
			unique: true,
		},
		password: {
			type: DataTypes.STRING(250),
			allowNull: false,
			field: 'password_user',
		},
		currency: {
			type: DataTypes.ENUM('ars', 'eur', 'usd'),
			allowNull: false,
			field: 'currency_user',
		},
		active: {
			type: DataTypes.BOOLEAN,
			allowNull: false,
			defaultValue: 1,
			field: 'active_row',
		},
		timestampCreation: {
			type: DataTypes.DATE,
			defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
			field: 'timestamp_creation_row',
		},
		timestampModify: {
			type: DataTypes.DATE,
			defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
			field: 'timestamp_modify_row',
		},
	},
	{
		tableName: 'tbl_users',
		timestamps: false,
	}
)

export default UserModel

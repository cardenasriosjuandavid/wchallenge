import jwt, { JwtPayload } from 'jsonwebtoken'

import AccessTokenManager from '../../../app/security/jwt'

class JwtAccessTokenManager extends AccessTokenManager {
	private JWT_SECRET_KEY

	constructor() {
		super()
		this.JWT_SECRET_KEY = process.env.JWT_SECRET_KEY || ''
	}

	generate(payload: string | object | Buffer): string {
		return jwt.sign(payload, this.JWT_SECRET_KEY, { expiresIn: '15m' })
	}

	decode(accessToken: string): string | JwtPayload {
		return jwt.verify(accessToken, this.JWT_SECRET_KEY)
	}
}

export default JwtAccessTokenManager

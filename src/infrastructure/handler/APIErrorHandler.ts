import BaseError from './BaseErrorHandler'
import HttpStatusCode from './HttpStatusCode'

class APIError extends BaseError {
	constructor(
		name = 'ERROR',
		httpCode = HttpStatusCode.INTERNAL_SERVER,
		description = 'Internal server error'
	) {
		super(name, httpCode, description)
	}
}

export default APIError

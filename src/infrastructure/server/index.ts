import express, { Application, NextFunction, Request, Response } from 'express'
import cors from 'cors'
import { Server as ServerHTTP } from 'http'

// Service Locator
import serviceLocator from '../config/service-locator'

// Routes
import AuthRouter from '../../interfaces/routes/Auth'
import UserRouter from '../../interfaces/routes/User'

// Handler
import BaseError from '../handler/BaseErrorHandler'
import CryptoRouter from '../../interfaces/routes/Crypto'

class Server {
	private app: Application

	public pathAPI = {
		root: '/api/v1',
		users: '/users',
		auth: '/auth',
		crypto: '/crypto',
	}

	constructor() {
		// Init express
		this.app = express()

		// Middlewares
		this.middlewares()

		// Routes
		this.routes()

		// Error Handler
		this.errorHandler()
	}

	middlewares(): void {
		this.app.use(cors())
		this.app.use(express.json())
		this.app.use((req, res, next) => {
			req.serviceLocator = serviceLocator
			next()
		})
	}

	errorHandler(): void {
		this.app.use(
			(err: Error, req: Request, res: Response, next: NextFunction) => {
				if (err instanceof BaseError) {
					res.status(err.httpCode).json({
						ErrorMessage: err.message,
						Error: err,
					})
				} else {
					res.status(500).json({ ErrorMessage: err.message })
				}
			}
		)
	}

	routes(): void {
		this.app.use(`${this.pathAPI.root + this.pathAPI.auth}`, AuthRouter)
		this.app.use(`${this.pathAPI.root + this.pathAPI.users}`, UserRouter)
		this.app.use(`${this.pathAPI.root + this.pathAPI.crypto}`, CryptoRouter)
	}

	listen(): ServerHTTP {
		return this.app.listen(process.env.PORT, () => {
			console.log(`Server on port ${process.env.PORT}`)
			console.log(`API: ${this.pathAPI.root}`)
		})
	}
}

export default Server

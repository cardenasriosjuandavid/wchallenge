// Respository
import UserEntity from '../../../domain/entities/User'

// Domain
import UserDomainRepository from '../../../domain/repositories/User'

class UserRepository extends UserDomainRepository {
	userExist: string

	messageSuccess: string

	password: string

	constructor() {
		super()
		this.userExist = 'johndoe'
		this.password = 'password'
		this.messageSuccess = 'successfully created'
	}

	getByUsername(username: string): UserEntity | null {
		if (username === this.userExist)
			return new UserEntity({
				currency: 'ars',
				username,
				password:
					'$2b$10$N5DmkSpdJVyCzlWnUKLUweoZ86/RWNSwduZTPa74ZS5zmSWrO3MZ6',
				name: 'John',
				surname: 'Doe',
			})
		return null
	}

	create({ username }: UserEntity): string {
		return `User ${username} ${this.messageSuccess}`
	}
}

export default UserRepository

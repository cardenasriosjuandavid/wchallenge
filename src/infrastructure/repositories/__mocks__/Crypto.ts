// Entity
import UserCryptoEntity from '../../../domain/entities/UserCrypto'

// Domain
import CryptoDomainRespository from '../../../domain/repositories/Crypto'

class CryptoRespository extends CryptoDomainRespository {
	idCrypto: string

	username: string

	constructor() {
		super()
		this.idCrypto = 'dogecoin'
		this.username = 'johndoe'
	}

	saveCrypto(userCrypt: UserCryptoEntity) {
		if (userCrypt.username === this.username) return userCrypt
		return false
	}

	getCryptoByUser(idCrypto: string, username: string) {
		if (idCrypto === this.idCrypto && username === this.username) {
			return true
		}
		return false
	}

	getTopCryptosByUser(username: string): UserCryptoEntity[] {
		if (username === this.username)
			return [
				{ id: 1, idCrypto: 'botcoin', username },
				{ id: 2, idCrypto: 'ethereum', username },
				{ id: 3, idCrypto: 'dogecoin', username },
			]
		return []
	}
}

export default CryptoRespository

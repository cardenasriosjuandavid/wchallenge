// Models
import UserCryptoModel from '../orm/sequelize/models/UserCrypto'

// Entity
import UserCryptoEntity from '../../domain/entities/UserCrypto'

// Domain
import CryptoDomainRespository from '../../domain/repositories/Crypto'

class CryptoRespository extends CryptoDomainRespository {
	private model = UserCryptoModel

	async saveCrypto(userCrypt: UserCryptoEntity): Promise<UserCryptoEntity> {
		const res = await this.model.create(userCrypt)
		return new UserCryptoEntity(res)
	}

	async getCryptoByUser(
		idCrypto: string,
		username: string
	): Promise<UserCryptoEntity | null> {
		const data = await this.model.findOne({
			where: { username, idCrypto },
			raw: true,
		})
		if (data) return new UserCryptoEntity(data)
		return null
	}

	async getTopCryptosByUser(username: string): Promise<UserCryptoEntity[]> {
		return this.model
			.findAll({ where: { username }, raw: true })
			.then((res) => res.map((data) => new UserCryptoEntity(data)))
	}
}

export default CryptoRespository

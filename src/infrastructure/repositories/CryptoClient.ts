import Axios from 'axios'

// Domain
import CryptoClientDomain from '../../domain/repositories/CryptoClient'

// @Types
import { ICoinsMarket } from '../../domain/types/CoinsMaket'
import { ISimplePrice } from '../../domain/types/SimplePrice'
import APIError from '../handler/APIErrorHandler'
import HttpStatusCode from '../handler/HttpStatusCode'

class CryptoClientRepository extends CryptoClientDomain {
	private clientRequest

	constructor() {
		super()
		this.clientRequest = Axios.create({
			baseURL: 'https://api.coingecko.com/api/v3',
		})
	}

	async getAll(currency: string, ids?: string): Promise<ICoinsMarket[]> {
		try {
			const res = await this.clientRequest.get<ICoinsMarket[]>(
				'/coins/markets',
				{
					params: { vs_currency: currency, ids },
				}
			)
			return res.data
		} catch (error) {
			throw new APIError(
				'NOT_FOUND',
				HttpStatusCode.NOT_FOUND,
				error.response.data.error
			)
		}
	}

	async getCryptoById(idCrypto: string): Promise<any> {
		try {
			const res = await this.clientRequest.get(`/coins/${idCrypto}`, {
				params: {
					localization: true,
					tickers: false,
					market_data: false,
					community_data: false,
					developer_data: false,
					sparkline: false,
				},
			})
			return res.data
		} catch (error) {
			throw new APIError(
				'NOT_FOUND',
				HttpStatusCode.NOT_FOUND,
				error.response.data.error
			)
		}
	}

	async getPrices(ids: string): Promise<ISimplePrice> {
		try {
			const res = await this.clientRequest.get<ISimplePrice>(
				'/simple/price',
				{
					params: { ids, vs_currencies: 'ars,usd,eur' },
				}
			)
			return res.data
		} catch (error) {
			throw new APIError(
				'NOT_FOUND',
				HttpStatusCode.NOT_FOUND,
				error.response.data.error
			)
		}
	}
}

export default CryptoClientRepository

// Model
import UserModel from '../orm/sequelize/models/Users'

// Entity
import UserEntity from '../../domain/entities/User'

// Respository
import UserDomainRepository from '../../domain/repositories/User'

class UserRepository extends UserDomainRepository {
	private model

	constructor() {
		super()
		this.model = UserModel
	}

	async create(user: UserEntity): Promise<UserEntity> {
		const res = await this.model.create(user)
		return new UserEntity(res)
	}

	async getByUsername(username: string): Promise<UserEntity | null> {
		const data = await this.model.findOne({
			where: { username, active: true },
			raw: true,
		})
		if (data) return new UserEntity(data)
		return null
	}
}

export default UserRepository

// Service locator
import serviceLocator from '../../../../infrastructure/config/service-locator'

// Use case
import Login from '../Login'

// Respositories
import UserRepository from '../../../../infrastructure/repositories/User'
import JwtAccessTokenManager from '../../../../infrastructure/security/jwt'

jest.mock('../../../../infrastructure/repositories/User')

describe('Use case: Login', () => {
	it('Invalid username or password', () => {
		return expect(() =>
			Login({ username: 'johndoe', password: 'invalid_password' }, {
				userRepository: new UserRepository(),
				accessTokenManager: new JwtAccessTokenManager(),
			} as typeof serviceLocator)
		).rejects.toThrow('Invalid username or password')
	})

	it('Successful login and token return', async () => {
		const exp = await Login({ username: 'johndoe', password: 'password' }, {
			userRepository: new UserRepository(),
			accessTokenManager: new JwtAccessTokenManager(),
		} as typeof serviceLocator)
		expect(exp).toContain('eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9')
	})
})

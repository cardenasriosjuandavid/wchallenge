import bcrypt from 'bcrypt'

// Service Locator
import serviceLocator from '../../../infrastructure/config/service-locator'

// Handle Error
import APIError from '../../../infrastructure/handler/APIErrorHandler'
import HttpStatusCode from '../../../infrastructure/handler/HttpStatusCode'

const Login = async (
	{ password, username: userName }: { username: string; password: string },
	{ accessTokenManager, userRepository }: typeof serviceLocator
): Promise<string> => {
	const user = await userRepository.getByUsername(userName)

	if (!user || !bcrypt.compareSync(password, user.password))
		throw new APIError(
			'INVALID_CREDENTIALS',
			HttpStatusCode.BAD_REQUEST,
			'Invalid username or password'
		)

	const { name, surname, username, currency } = user

	return accessTokenManager.generate({ name, surname, username, currency })
}

export default Login

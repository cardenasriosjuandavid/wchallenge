// Service Locator
import serviceLocator from '../../../../infrastructure/config/service-locator'

// Respository
import UserRepository from '../../../../infrastructure/repositories/User'

// Use Cases
import CreateUser from '../CreateUser'

jest.mock('../../../../infrastructure/repositories/User')

describe('Use Case: Create User', () => {
	it('Existing user', () => {
		return expect(() =>
			CreateUser(
				{
					name: 'John',
					surname: 'Doe',
					currency: 'usd',
					password: 'password',
					username: 'johndoe',
				},
				{
					userRepository: new UserRepository(),
				} as typeof serviceLocator
			)
		).rejects.toThrow('This user is already registered')
	})

	it('Successful user creation', async () => {
		const result = await CreateUser(
			{
				name: 'Jane',
				surname: 'Doe',
				currency: 'usd',
				password: 'password',
				username: 'janedoe',
			},
			{
				userRepository: new UserRepository(),
			} as typeof serviceLocator
		)

		expect(result).toBe(`User janedoe successfully created`)
	})
})

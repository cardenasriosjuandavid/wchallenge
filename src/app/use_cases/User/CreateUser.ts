import bcrypt from 'bcrypt'

// Service Locator
import serviceLocator from '../../../infrastructure/config/service-locator'

// Entities
import UserEntity from '../../../domain/entities/User'

// Handler
import APIError from '../../../infrastructure/handler/APIErrorHandler'
import HttpStatusCode from '../../../infrastructure/handler/HttpStatusCode'

// @Types
import { IUser } from '../../../domain/types/User'

const createUser = async (
	user: IUser,
	{ userRepository }: typeof serviceLocator
): Promise<UserEntity> => {
	const exist = await userRepository.getByUsername(user.username)

	if (exist)
		throw new APIError(
			'USER_EXIST',
			HttpStatusCode.BAD_REQUEST,
			'This user is already registered'
		)

	return userRepository.create(
		new UserEntity({
			...user,
			password: bcrypt.hashSync(user.password, bcrypt.genSaltSync()),
		})
	)
}

export default createUser

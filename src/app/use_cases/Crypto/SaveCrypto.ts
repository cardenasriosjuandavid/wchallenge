// Service Locator
import serviceLocator from '../../../infrastructure/config/service-locator'

// Entities
import UserCryptoEntity from '../../../domain/entities/UserCrypto'

// Error Handler
import APIError from '../../../infrastructure/handler/APIErrorHandler'
import HttpStatusCode from '../../../infrastructure/handler/HttpStatusCode'

const SaveCrypto = async (
	idCrypto: string,
	username: string,
	{ cryptoRespository, cryptoClientRepository }: typeof serviceLocator
): Promise<UserCryptoEntity> => {
	await cryptoClientRepository.getCryptoById(idCrypto)

	const existCrypto = await cryptoRespository.getCryptoByUser(
		idCrypto,
		username
	)

	if (existCrypto) {
		throw new APIError(
			'CRYPTO_ALREADY_EXIST',
			HttpStatusCode.BAD_REQUEST,
			'The cryptocurrency already registered for this user'
		)
	}

	return cryptoRespository.saveCrypto(
		new UserCryptoEntity({
			idCrypto,
			username,
		})
	)
}

export default SaveCrypto

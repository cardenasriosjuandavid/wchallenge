// Service Locator
import serviceLocator from '../../../infrastructure/config/service-locator'

interface Response {
	currentPrice: number
	id: string
	image: string
	symbol: string
	name: string
	lastUpdate: string
}

const GetAllCryptos = (
	currency: string,
	{ cryptoClientRepository }: typeof serviceLocator
): Promise<Response[]> =>
	cryptoClientRepository.getAll(currency).then((res) =>
		res.map(
			({
				current_price: currentPrice,
				id,
				image,
				symbol,
				name,
				last_updated: lastUpdate,
			}) => ({
				currentPrice,
				id,
				image,
				symbol,
				name,
				lastUpdate,
			})
		)
	)

export default GetAllCryptos

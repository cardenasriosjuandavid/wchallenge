import serviceLocator from '../../../../infrastructure/config/service-locator'
import CryptoRespository from '../../../../infrastructure/repositories/Crypto'
import CryptoClientRepository from '../../../../infrastructure/repositories/CryptoClient'

// Use case
import SaveCrypto from '../SaveCrypto'

jest.mock('../../../../infrastructure/repositories/Crypto')

describe('Use Case: Save Crypto', () => {
	it('Cryptocurrency already registered', () => {
		return expect(() =>
			SaveCrypto('dogecoin', 'johndoe', {
				cryptoRespository: new CryptoRespository(),
				cryptoClientRepository: new CryptoClientRepository(),
			} as typeof serviceLocator)
		).rejects.toThrow('The cryptocurrency already registered for this user')
	})

	it('Validation if the cryptocurrency exists', () => {
		return expect(() =>
			SaveCrypto('invalidCrypto', 'johndoe', {
				cryptoRespository: new CryptoRespository(),
				cryptoClientRepository: new CryptoClientRepository(),
			} as typeof serviceLocator)
		).rejects.toThrow('Could not find coin with the given id')
	})

	it('Registro exitoso', async () => {
		const coin = await SaveCrypto('bitcoin', 'johndoe', {
			cryptoRespository: new CryptoRespository(),
			cryptoClientRepository: new CryptoClientRepository(),
		} as typeof serviceLocator)

		expect(coin).toMatchObject({
			idCrypto: 'bitcoin',
			username: 'johndoe',
		})
	})
})

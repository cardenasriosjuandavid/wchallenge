// Service Locator
import serviceLocator from '../../../../infrastructure/config/service-locator'

// Respositories
import CryptoRespository from '../../../../infrastructure/repositories/Crypto'
import CryptoClientRepository from '../../../../infrastructure/repositories/CryptoClient'

// Use case
import ListTopCryptosByUser from '../ListTopCryptosByUser'

jest.mock('../../../../infrastructure/repositories/Crypto')

describe('Use Case: ListTopCryptosByUser', () => {
	it('Valid size cryptos', async () => {
		const exp = await ListTopCryptosByUser('johndoe', 'usd', {
			cryptoRespository: new CryptoRespository(),
			cryptoClientRepository: new CryptoClientRepository(),
		} as typeof serviceLocator)
		expect(exp.length).toBeTruthy()
	})

	it('No have cryptos', async () => {
		const exp = await ListTopCryptosByUser('janedoe', 'usd', {
			cryptoRespository: new CryptoRespository(),
			cryptoClientRepository: new CryptoClientRepository(),
		} as typeof serviceLocator)

		expect(exp.length).toBeFalsy()
	})
})

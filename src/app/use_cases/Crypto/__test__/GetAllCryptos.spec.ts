// Service locator
import serviceLocator from '../../../../infrastructure/config/service-locator'

// Repository
import CryptoClientRepository from '../../../../infrastructure/repositories/CryptoClient'

// Use case
import GetAllCryptos from '../GetAllCryptos'

describe('Use case: GetAllCryptos', () => {
	it('Get ars cryptos', async () => {
		const exp = await GetAllCryptos('ars', {
			cryptoClientRepository: new CryptoClientRepository(),
		} as typeof serviceLocator)
		expect(exp.length > 0).toBeTruthy()
	})

	it('Get usd cryptos', async () => {
		const exp = await GetAllCryptos('usd', {
			cryptoClientRepository: new CryptoClientRepository(),
		} as typeof serviceLocator)
		expect(exp.length > 0).toBeTruthy()
	})

	it('Get eur cryptos', async () => {
		const exp = await GetAllCryptos('eur', {
			cryptoClientRepository: new CryptoClientRepository(),
		} as typeof serviceLocator)
		expect(exp.length > 0).toBeTruthy()
	})

	it('Get invalid cryptos', async () => {
		return expect(() =>
			GetAllCryptos('invalid', {
				cryptoClientRepository: new CryptoClientRepository(),
			} as typeof serviceLocator)
		).rejects.toThrow('invalid vs_currency')
	})
})

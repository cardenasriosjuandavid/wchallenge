// Service Locator
import serviceLocator from '../../../infrastructure/config/service-locator'

interface Response {
	symbol: string
	ars: number
	usd: number
	eur: number
	name: string
	image: string
	lastUpdated: string
}
const ListTopCryptosByUser = async (
	username: string,
	currency: string,
	{ cryptoRespository, cryptoClientRepository }: typeof serviceLocator
): Promise<Response[]> => {
	const myCryptos = await cryptoRespository.getTopCryptosByUser(username)

	if (!myCryptos.length) return []

	const ids = myCryptos.map((item) => item.idCrypto).join()

	const [prices, TopCrytpos] = await Promise.all([
		cryptoClientRepository.getPrices(ids),
		cryptoClientRepository.getAll(currency, ids),
	])

	return TopCrytpos.reduce((acum, i) => {
		const findDetail = myCryptos.find(({ idCrypto }) => idCrypto === i.id)
		if (findDetail && prices[i.id]) {
			const { symbol, last_updated: lastUpdated, name, image } = i
			acum.push({
				symbol,
				...prices[i.id],
				name,
				image,
				lastUpdated,
			})
		}
		return acum
	}, [] as Response[])
}

export default ListTopCryptosByUser

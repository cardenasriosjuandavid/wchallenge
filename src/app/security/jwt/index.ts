class AccessTokenManager {
	private error: string

	constructor() {
		this.error = 'ERR_METHOD_NOT_IMPLEMENTED'
	}

	// eslint-disable-next-line @typescript-eslint/no-unused-vars
	generate(payload: string | Buffer): void {
		throw new Error(this.error)
	}

	// eslint-disable-next-line @typescript-eslint/no-unused-vars
	decode(accessToken: string): void {
		throw new Error(this.error)
	}
}

export default AccessTokenManager

import { NextFunction, Request, Response } from 'express'

// Use Case
import Login from '../../../app/use_cases/Auth/Login'

const login = async (
	req: Request,
	res: Response,
	next: NextFunction
): Promise<void> => {
	try {
		res.json({ token: await Login(req.body, req.serviceLocator) })
	} catch (error) {
		next(error)
	}
}

export default {
	login,
}

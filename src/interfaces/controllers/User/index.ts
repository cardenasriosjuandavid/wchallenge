import { NextFunction, Request, Response } from 'express'

// Use cases
import CreateUser from '../../../app/use_cases/User/CreateUser'

const createUser = async (
	req: Request,
	res: Response,
	next: NextFunction
): Promise<void> => {
	try {
		const { username } = await CreateUser(req.body, req.serviceLocator)
		res.status(201).json({
			message: `User ${username} created successfully`,
		})
	} catch (error) {
		next(error)
	}
}

export default {
	createUser,
}

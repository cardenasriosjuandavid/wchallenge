import { Request, Response, NextFunction } from 'express'

// Use Cases
import GetAllCryptos from '../../../app/use_cases/Crypto/GetAllCryptos'
import ListTopCryptosByUser from '../../../app/use_cases/Crypto/ListTopCryptosByUser'
import SaveCrypto from '../../../app/use_cases/Crypto/SaveCrypto'

// Entitiess
import UserEntity from '../../../domain/entities/User'

const getAll = async (
	req: Request,
	res: Response,
	next: NextFunction
): Promise<void> => {
	try {
		const { currency } = req.serviceLocator.accessTokenManager.decode(
			req.header('Authorization') || ''
		) as UserEntity

		const listCryptos = await GetAllCryptos(currency, req.serviceLocator)

		res.json({
			message: 'Search successfully!',
			listCryptos,
			totalItems: listCryptos.length,
		})
	} catch (error) {
		next(error)
	}
}

const saveCrypto = async (
	req: Request,
	res: Response,
	next: NextFunction
): Promise<void> => {
	try {
		const { username } = req.serviceLocator.accessTokenManager.decode(
			req.header('Authorization') || ''
		) as UserEntity

		const { idCrypto } = await SaveCrypto(
			req.body.idCrypto,
			username,
			req.serviceLocator
		)

		res.status(201).json({ message: `${idCrypto} was successfully stored` })
	} catch (error) {
		next(error)
	}
}

const getTopCryptosByUser = async (
	req: Request,
	res: Response,
	next: NextFunction
): Promise<void> => {
	try {
		const { username, currency } =
			req.serviceLocator.accessTokenManager.decode(
				req.header('Authorization') || ''
			) as UserEntity

		const listTop = await ListTopCryptosByUser(
			username,
			currency,
			req.serviceLocator
		)

		res.json({ message: 'Successful search', listTop })
	} catch (error) {
		next(error)
	}
}

export default {
	getAll,
	saveCrypto,
	getTopCryptosByUser,
}

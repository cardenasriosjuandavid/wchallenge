import { Schema } from 'express-validator'

const LoginSchema: Schema = {
	username: {
		in: 'body',
		isString: true,
		isLength: {
			options: { min: 3 },
			errorMessage:
				'It must have a minimum of 3 characters and a maximum of 50.',
		},
	},
	password: {
		in: 'body',
		isString: true,
		isLength: {
			options: { min: 5 },
			errorMessage: 'It must be at least 5 characters long.',
		},
	},
}

export default LoginSchema

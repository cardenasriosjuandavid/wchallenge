import { Schema } from 'express-validator'

const UserSchema: Schema = {
	name: {
		in: 'body',
		isString: true,
		isLength: {
			options: { min: 1, max: 50 },
			errorMessage:
				'It must be a minimum of 1 character and a maximum of 50 characters.',
		},
	},
	surname: {
		in: 'body',
		isString: true,
		isLength: {
			options: { min: 1 },
			errorMessage:
				'It must be a minimum of 1 character and a maximum of 50 characters.',
		},
	},
	username: {
		in: 'body',
		isString: true,
		isLength: {
			options: { min: 3 },
			errorMessage:
				'It must have a minimum of 3 characters and a maximum of 50.',
		},
	},
	password: {
		in: 'body',
		isString: true,
		isLength: {
			options: { min: 5 },
			errorMessage: 'It must be at least 5 characters long.',
		},
	},
	currency: {
		in: 'body',
		isString: true,
		isIn: {
			options: [['ars', 'eur', 'usd']],
			errorMessage:
				"Invalid currency, only 'ars', 'usd' and 'eur' are allowed.",
		},
	},
}

export default UserSchema

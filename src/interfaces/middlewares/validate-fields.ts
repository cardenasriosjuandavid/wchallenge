import { Request, Response, NextFunction } from 'express'
import { validationResult } from 'express-validator'

const validateFields = (
	req: Request,
	res: Response,
	next: NextFunction
): void => {
	const errors = validationResult(req)

	if (!errors.isEmpty()) {
		res.status(400).json(errors.array())
		return
	}

	next()
}

export default validateFields

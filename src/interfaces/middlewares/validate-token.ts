import { Request, Response, NextFunction } from 'express'

// Entities
import UserEntity from '../../domain/entities/User'

// Handle Errors
import APIError from '../../infrastructure/handler/APIErrorHandler'
import HttpStatusCode from '../../infrastructure/handler/HttpStatusCode'

const validateToken = async (
	req: Request,
	res: Response,
	next: NextFunction
): Promise<void> => {
	try {
		const token = req.header('Authorization')

		if (!token)
			throw new APIError(
				'INVALID_TOKEN',
				HttpStatusCode.UNAUTHORIZED,
				'Invalid Token'
			)

		let tokenInfo

		try {
			tokenInfo = req.serviceLocator.accessTokenManager.decode(
				token
			) as UserEntity
		} catch (error) {
			throw new APIError(
				'INVALID_TOKEN',
				HttpStatusCode.UNAUTHORIZED,
				'Invalid Token'
			)
		}

		const user = await req.serviceLocator.userRepository.getByUsername(
			tokenInfo.username
		)

		if (!user) {
			throw new APIError(
				'INVALID_USER',
				HttpStatusCode.NOT_FOUND,
				'User is not registered'
			)
		}

		next()
	} catch (error) {
		next(error)
	}
}

export default validateToken

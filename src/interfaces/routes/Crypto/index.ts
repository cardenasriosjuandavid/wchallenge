import { Router } from 'express'
import { checkSchema } from 'express-validator'

import Controller from '../../controllers/Crypto'
import validateFields from '../../middlewares/validate-fields'
import validateToken from '../../middlewares/validate-token'

const CryptoRouter = Router()

CryptoRouter.get('/all', validateToken, Controller.getAll)
CryptoRouter.post(
	'/save',
	checkSchema({
		idCrypto: {
			isString: true,
			isLength: {
				options: { min: 1 },
				errorMessage: 'It must be a minimum of 1 character',
			},
		},
	}),
	validateFields,
	validateToken,
	Controller.saveCrypto
)
CryptoRouter.get('/top', validateToken, Controller.getTopCryptosByUser)

export default CryptoRouter

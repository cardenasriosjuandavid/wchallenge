import { Router } from 'express'
import { checkSchema } from 'express-validator'

// Controller
import Controller from '../../controllers/User'

// Middlewares | Schemas
import UserSchema from '../../middlewares/Schema/User'
import validateFields from '../../middlewares/validate-fields'

const UserRouter = Router()

UserRouter.post(
	'/create',
	checkSchema(UserSchema),
	validateFields,
	Controller.createUser
)

export default UserRouter

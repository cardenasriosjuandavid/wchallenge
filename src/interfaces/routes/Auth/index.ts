import { Router } from 'express'
import { checkSchema } from 'express-validator'

import AuthController from '../../controllers/Auth'
import LoginSchema from '../../middlewares/Schema/Auth'
import validateFields from '../../middlewares/validate-fields'

const AuthRouter = Router()

AuthRouter.post(
	'/login',
	checkSchema(LoginSchema),
	validateFields,
	AuthController.login
)

export default AuthRouter

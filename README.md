# WChallenge Cryptocurrency - Technical Tests 👾

## Getting Started
▶️ [Documentation of all requests](https://documenter.getpostman.com/view/4605593/TzecCkNb) in postman

> **Note:** Create an .env file in the root of the project and add the environment variables sent to the email.

Execute the following command, it will download the project dependencies and then start the application on port `3000`

```bash
yarn install && yarn start
```

## Unit test coverage

Run the following command to run the unit tests and get the coverage report.

```bash
yarn test
```

![](img/coverage.png)

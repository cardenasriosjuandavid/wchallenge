import * as express from 'express'

import serviceLocator from '../src/infrastructure/config/service-locator'

declare global {
	namespace Express {
		interface Request {
			serviceLocator: typeof serviceLocator
		}
	}
}

module.exports = {
	preset: 'ts-jest',
	testEnvironment: 'node',
	coveragePathIgnorePatterns: [
		'<rootDir>/dist',
		'<rootDir>/src/app/security',
		'<rootDir>/src/infrastructure',
		'<rootDir>/src/interfaces',
	],
	setupFiles: ['dotenv/config'],
}

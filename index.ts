import Server from './src/infrastructure/server'
import bootstrap from './src/infrastructure/config/bootstrap'

const main = async () => {
	try {
		await bootstrap.init()
		const server = new Server()
		server.listen()
	} catch (error) {
		console.error(error)
		process.exit(1)
	}
}

main()
